//
//  AppDelegate.swift
//  flightschedule
//
//  Created by Justin Andros on 3/30/16.
//  Copyright © 2016 Justin Andros. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}
